package com.lawman.order.server.service.impl;

import com.lawman.order.client.dto.request.OrderFilterRequest;
import com.lawman.order.client.dto.request.OrderStatusRequest;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.dto.response.common.PageResponse;
import com.lawman.order.client.enums.OrderStatus;
import com.lawman.order.server.entity.Order;
import com.lawman.order.server.exception.order.OrderNotFoundException;
import com.lawman.order.server.repository.OrderRepository;
import com.lawman.order.server.service.OrderService;
import com.lawman.order.server.service.base.impl.BaseServiceImpl;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;

import java.util.List;

@Slf4j
public class OrderServiceImpl extends BaseServiceImpl<Order> implements OrderService {
  private final OrderRepository repository;

  public OrderServiceImpl(OrderRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public OrderResponse createOrder(Order order) {
    log.info("(createOrder) order: {}", order);

    Order saveOrder = repository.save(order);

    return setOrderResponse(saveOrder);
  }

  @Override
  public OrderResponse updateShipmentId(String orderId, String shipmentId) {
    log.info("(updateShipment) orderId: {}, shipmentId: {}", orderId, shipmentId);

    repository.updateShipmentId(orderId, shipmentId);
    return null;
  }

  @Override
  public OrderResponse detail(String id) {
    log.info("(detail) id: {}", id);

    Order order = repository.findById(id).orElseThrow(OrderNotFoundException::new);

    return setOrderResponse(order);
  }

  @Override
  @Transactional
  public void remove(String id) {
    log.info("(remove) id: {}", id);

    if (!repository.existsById(id)) {
      throw new OrderNotFoundException();
    }
    repository.removeById(id);
  }

  @Override
  @Transactional
  public void updateOrderStatus(String id, OrderStatusRequest request) {
    log.info("(updateOrderStatus) id: {}, status: {}", id, request.getOrderStatus());

    OrderStatus status = OrderStatus.of(request.getOrderStatus());

    if (!repository.existsById(id)) {
      throw new OrderNotFoundException();
    }

    repository.updateOrderStatus(id, status);
  }

  @Override
  public PageResponse<OrderResponse> filter(OrderFilterRequest request) {
    log.info("(filter) request: {}", request);

    OrderStatus status = OrderStatus.of(request.getStatus());

    List<String> productIds = request.getProductIds().isEmpty() ? null : request.getProductIds();
    List<String> sellerIds = request.getSellerIds().isEmpty() ? null : request.getSellerIds();

    List<OrderResponse> orderResponses = repository.filter(
          productIds,
          sellerIds,
          request.getCustomerId(),
          status,
          request.getStartDate(),
          request.getEndDate(),
          PageRequest.of(0, 5)
    );

    int countFilter = repository.countFilter(
          productIds,
          sellerIds,
          request.getCustomerId(),
          status,
          request.getStartDate(),
          request.getEndDate()
    );

    return PageResponse.of(orderResponses, countFilter);
  }

  private OrderResponse setOrderResponse(Order order) {
    OrderResponse response = new OrderResponse();
    response.setId(order.getId());
    if (order.getCustomerId() != null) response.setCustomerId(order.getCustomerId());
    if (order.getTotalAmount() != null) response.setTotalAmount(order.getTotalAmount());
    if (order.getShipmentId() != null) response.setShipmentId(order.getShipmentId());
    if (order.getCode() != null) response.setCode(order.getCode());
    if (order.getPaymentId() != null) response.setPaymentId(order.getPaymentId());
    if (order.getStatus() != null) response.setStatus(order.getStatus().getValue());
    return response;
  }
}
