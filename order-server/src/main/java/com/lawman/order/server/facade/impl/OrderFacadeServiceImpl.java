package com.lawman.order.server.facade.impl;

import com.lawman.order.client.dto.request.*;
import com.lawman.order.client.dto.response.OrderDetailResponse;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.client.dto.response.common.PageResponse;
import com.lawman.order.client.dto.response.payment.PaymentResponse;
import com.lawman.order.client.dto.response.product.ProductItemResponse;
import com.lawman.order.client.dto.response.product.ProductResponse;
import com.lawman.order.client.dto.response.shipment.ShipmentResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.entity.Order;
import com.lawman.order.server.entity.ProductOrder;
import com.lawman.order.server.exception.order.*;
import com.lawman.order.server.facade.OrderFacadeService;
import com.lawman.order.server.service.OrderService;
import com.lawman.order.server.service.ProductOrderService;
import com.lawman.order.server.service.routes.PaymentService;
import com.lawman.order.server.service.routes.ProductService;
import com.lawman.order.server.service.routes.ShipmentService;
import com.lawman.order.server.service.routes.UserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class OrderFacadeServiceImpl implements OrderFacadeService {
  private final OrderService orderService;
  private final ProductOrderService productOrderService;
  private final UserService userService;
  private final ProductService productService;
  private final PaymentService paymentService;
  private final ShipmentService shipmentService;

  @Override
  @Transactional
  public OrderResponse createOrder(OrderRequest request) {
    log.info("(createOrder) request: {}", request);

    double totalAmount = 0.0;

    for (ProductOrderRequest productOrder : request.getProductOrders()) {
      if (productOrder.getProductItemId() == null) {
        totalAmount += (productOrder.getPrice() * productOrder.getQuantity());
      } else {
        ProductItemResponse productItemResponse = productService
              .detailProductItem(productOrder
                    .getProductItemId()).getData();

        totalAmount += (productItemResponse.getSellingPrice() * productItemResponse.getQuantity());
      }
    }

    Order order = new Order();
    order.setCustomerId(request.getCustomerId());
    order.setTotalAmount(totalAmount);

    try {
      log.debug("(call checkUserExists) customerId: {}", request.getCustomerId());

      ResponseGeneral<Void> response = userService.checkUserExists(request.getCustomerId());
      if (response.getStatus() != HttpStatus.OK.value()) throw new SellerNotFoundException();
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    try {
      log.debug("(call checkCart) ProductOrders: {}", request.getProductOrders());

      ResponseGeneral<Void> response = productService.deDuct(request.getProductOrders());
      if (response.getStatus() != HttpStatus.OK.value()) throw new ProductServiceCheckFailedException();
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    try {
      log.debug("(call createPayment) request: {}", request);

      ResponseGeneral<String> response = paymentService.createPayment(request);
      if (response.getStatus() != HttpStatus.OK.value()) throw new PaymentServiceCheckFailed();
      order.setPaymentId(response.getData());
    } catch (Exception e) {
      productService.revertCart(request.getProductOrders());
      throw new ConnectServiceFailedException();
    }

    try {
      log.debug("(call createShipment) request: {}", request);

      ResponseGeneral<String> response = shipmentService.createShipment(request);
      if (response.getStatus() != HttpStatus.OK.value()) throw new ShipmentServiceCheckFailed();
      order.setShipmentId(response.getData());
    } catch (Exception e) {
      paymentService.revertPayment(request);
      productService.revertCart(request.getProductOrders());
      throw new ConnectServiceFailedException();
    }

    OrderResponse orderResponse = orderService.createOrder(order);
    List<ProductOrder> productOrders = productOrderService.createProductOrders(
          request.getProductOrders(),
          orderResponse.getId());

    try {
      log.debug("(call details) ProductOrders: {}", productOrders);

      ResponseGeneral<List<ProductOrderDetailResponse>> response = productService.details(productOrders);
      if (response.getStatus() != HttpStatus.OK.value()) throw new ProductServiceCheckFailedException();
      orderResponse.setProductOrderDetails(response.getData());
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    return orderResponse;
  }

  @Override
  @Transactional
  public OrderDetailResponse updateOrder(OrderUpdateRequest updateRequest, String orderId) {
    log.info("(updateOrder) updateRequest: {}, orderId: {}", updateRequest, orderId);

    OrderResponse orderExist = orderService.detail(orderId);
    if (orderExist.getStatus() != 0) throw new CanNotUpdateOrderException();

    OrderDetailResponse orderDetailResponse = new OrderDetailResponse();
    setOrderDetail(orderDetailResponse, orderExist);
    List<ProductOrderRequest> productOrders = updateRequest.getProductOrders();

    List<ProductOrderDetailResponse> productOrderDetailResponses = this.updateProductOrder(productOrders, orderId);
    orderDetailResponse.setProductOrders(productOrderDetailResponses);

    ShipmentResponse shipmentResponse;

    try {
      log.debug("(call updateShipment) updateRequest: {}", updateRequest);

      ShipmentRequest shipmentRequest = new ShipmentRequest(
            orderId,
            updateRequest.getShippingName(),
            updateRequest.getShippingMethod(),
            updateRequest.getFromAddress(),
            updateRequest.getToAddress());

      shipmentResponse = shipmentService.createShipmentOrigin(shipmentRequest).getData();
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    orderDetailResponse.setShipmentResponse(shipmentResponse);
    orderService.updateShipmentId(orderId, shipmentResponse.getId());

    try {
      log.debug("(detailPayment) paymentId: {}", orderExist.getPaymentId());
      PaymentResponse paymentResponse = paymentService.detail(orderExist.getPaymentId()).getData();
      orderDetailResponse.setPaymentResponse(paymentResponse);
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    return orderDetailResponse;
  }

  @Override
  @Transactional
  public OrderDetailResponse detailOrder(String orderId) {
    log.info("(detailOrder) orderId: {}", orderId);

    OrderResponse orderResponse = orderService.detail(orderId);
    OrderDetailResponse response = new OrderDetailResponse();

    List<ProductOrderDetailResponse> productOrderDetailResponses = productOrderService
          .getByOrderId(orderResponse.getId());

    List<String> idDetails = new ArrayList<>();
    for (ProductOrderDetailResponse productOrderDetailResponse : productOrderDetailResponses) {
      idDetails.add(
            productOrderDetailResponse.getProductId() == null ?
                  productOrderDetailResponse.getProductItemId() :
                  productOrderDetailResponse.getProductId());
    }

    List<ProductResponse> productResponses = productService.getProductByIds(idDetails).getData();
    Map<String, ProductResponse> productResponseMap = productResponses.stream()
          .collect(Collectors.toMap(
                productResponse -> productResponse.getProductId() != null ?
                      productResponse.getProductId() : productResponse.getProductItemId(),
                productResponse -> productResponse
          ));

    productOrderDetailResponses.forEach(
          productOrderDetailResponse -> {
            String key = productOrderDetailResponse.getProductId() != null ?
                  productOrderDetailResponse.getOrderId() : productOrderDetailResponse.getProductItemId();
            ProductResponse productResponse = productResponseMap.get(key);
            if (productResponse != null) {
              productOrderDetailResponse.setName(productResponse.getProductName());
              productOrderDetailResponse.setColor(productResponse.getColor());
            }
          }
    );

    response.setProductOrders(productOrderDetailResponses);

    try {
      log.debug("(detailPayment) paymentId: {}", orderResponse.getPaymentId());
      PaymentResponse paymentResponse = paymentService.detail(orderResponse.getPaymentId()).getData();
      response.setPaymentResponse(paymentResponse);
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    try {
      log.debug("(detailShipment) shipmentId: {}", orderResponse.getShipmentId());
      ShipmentResponse shipmentResponse = shipmentService.detail(orderResponse.getShipmentId()).getData();
      response.setShipmentResponse(shipmentResponse);
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    setOrderDetail(response, orderResponse);

    return response;
  }

  @Override
  public PageResponse<OrderDetailResponse> filterOrder(OrderFacadeFilterRequest request) {
    log.info("(filterOrder) request: {}", request);

    List<String> productIds = new ArrayList<>();
    List<String> sellerIds = new ArrayList<>();

    List<ProductOrderDetailResponse> productOrderDetailResponses;

    try {
      log.debug("(find productIds by keyword) keyword: {}", request.getKeyword());
      productOrderDetailResponses = productService.findByKeyword(request.getKeyword()).getData();
      if (productOrderDetailResponses != null && !productOrderDetailResponses.isEmpty()) {
        productOrderDetailResponses.forEach(
              response -> {
                productIds.add(response.getProductId());
                productIds.add(response.getProductItemId());
                sellerIds.add(response.getSellerId());
              }
        );
      }
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    OrderFilterRequest orderFilterRequest = OrderFilterRequest.of(
          productIds,
          sellerIds,
          request.getCustomerId(),
          request.getStatus(),
          request.getStartDate(),
          request.getEndDate()
    );

    PageResponse<OrderResponse> orderPageResponse = orderService.filter(orderFilterRequest);

    List<OrderResponse> orderResponses = orderPageResponse.getList();

    List<String> paymentIds = new ArrayList<>();
    List<String> shipmentIds = new ArrayList<>();
    orderResponses.forEach(orderResponse -> {
      shipmentIds.add(orderResponse.getShipmentId());
      paymentIds.add(orderResponse.getPaymentId());
    });

    List<PaymentResponse> paymentResponses;
    List<ShipmentResponse> shipmentResponses;

    try {
      log.debug("(list payment by paymentIds) paymentIds: {}", paymentIds);
      paymentResponses = paymentService.findByIds(paymentIds).getData();
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    try {
      log.debug("(list payment by paymentIds) paymentIds: {}", paymentIds);
      shipmentResponses = shipmentService.findByIds(shipmentIds).getData();
    } catch (Exception e) {
      throw new ConnectServiceFailedException();
    }

    List<OrderDetailResponse> orderDetailResponses = this.setOrderDetailResponses(
          orderResponses,
          paymentResponses,
          shipmentResponses,
          productOrderDetailResponses
    );

    return PageResponse.of(
          orderDetailResponses,
          orderPageResponse.getCount()
    );
  }

  private void setOrderDetail(OrderDetailResponse response, OrderResponse orderResponse) {
    response.setId(orderResponse.getId());
    if (orderResponse.getCustomerId() != null) response.setCustomerId(orderResponse.getCustomerId());
    if (orderResponse.getTotalAmount() != null) response.setTotalAmount(orderResponse.getTotalAmount());
    if (orderResponse.getCode() != null) response.setCode(orderResponse.getCode());
    if (orderResponse.getStatus() != null) response.setStatus(orderResponse.getStatus());
  }

  private List<OrderDetailResponse> setOrderDetailResponses(
        List<OrderResponse> orderResponses,
        List<PaymentResponse> paymentResponses,
        List<ShipmentResponse> shipmentResponses,
        List<ProductOrderDetailResponse> productOrderDetailResponses
  ) {

    return orderResponses.stream()
          .map(orderResponse -> {
            OrderDetailResponse orderDetailResponse = new OrderDetailResponse();
            this.setOrderDetail(orderDetailResponse, orderResponse);
            orderDetailResponse.setProductOrders(
                  this.setFieldIfIdMatches(
                        productOrderService.getByOrderId(orderDetailResponse.getId()),
                        productOrderDetailResponses));
            orderDetailResponse.setPaymentResponse(
                  this.findPaymentInList(orderResponse.getPaymentId(),
                        paymentResponses));
            orderDetailResponse.setShipmentResponse(
                  this.findShipmentInList(orderResponse.getShipmentId(),
                        shipmentResponses));
            return orderDetailResponse;
          })
          .collect(Collectors.toList());
  }

  private PaymentResponse findPaymentInList(String paymentId, List<PaymentResponse> paymentResponses) {
    for (PaymentResponse paymentResponse : paymentResponses) {
      if (paymentResponse.getId().equals(paymentId)) {
        return paymentResponse;
      }
    }
    return null;
  }

  private ShipmentResponse findShipmentInList(String shipmentId, List<ShipmentResponse> shipmentResponses) {
    for (ShipmentResponse shipmentResponse : shipmentResponses) {
      if (shipmentResponse.getId().equals(shipmentId)) {
        return shipmentResponse;
      }
    }
    return null;
  }

  private List<ProductOrderDetailResponse> setFieldIfIdMatches(List<ProductOrderDetailResponse> desList, List<ProductOrderDetailResponse> fromList) {
    fromList.forEach(from ->
          desList.stream()
                .filter(fullResponse -> from.getId().equals(fullResponse.getId()))
                .findFirst()
                .ifPresent(matchedResponse -> {
                  matchedResponse.setName(from.getName());
                  matchedResponse.setColor(from.getColor());
                })
    );

    return desList;
  }

  private List<ProductOrderDetailResponse> updateProductOrder(List<ProductOrderRequest> productOrders, String orderId) {
    log.info("(updateProductOrder) productOrders: {}, orderId: {}", productOrders, orderId);

    List<ProductOrder> productOrdersUpdate = new ArrayList<>();
    List<ProductOrderRequest> productOrdersCreate = new ArrayList<>();
    List<String> productOrderIds = new ArrayList<>();

    for (ProductOrderRequest productOrder : productOrders) {
      String productOrderId = productOrderService.getIdByItemId(
            productOrder.getProductId(),
            productOrder.getProductItemId(),
            orderId);

      productOrderIds.add(productOrderId);

      if (productOrderId != null) {
        ProductOrder productOrderUpdate = productOrderService.getById(productOrderId);
        productOrderUpdate.setProductId(productOrder.getProductId());
        productOrderUpdate.setProductItemId(productOrder.getProductItemId());
        productOrderUpdate.setQuantity(productOrder.getQuantity());
        productOrderUpdate.setPrice(productOrder.getPrice());
        productOrderUpdate.setSellerId(productOrder.getSellerId());
        productOrdersUpdate.add(productOrderUpdate);
      } else {
        productOrdersCreate.add(productOrder);
      }
    }

    productOrderService.saveProductOrders(productOrdersUpdate);
    productOrderService.createProductOrders(productOrdersCreate, orderId);

    return productOrderService.getByIds(productOrderIds);
  }
}
