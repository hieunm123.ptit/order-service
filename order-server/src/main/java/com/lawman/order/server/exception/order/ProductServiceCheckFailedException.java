package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.BadRequestException;

public class ProductServiceCheckFailedException extends BadRequestException {
  public ProductServiceCheckFailedException(){
    setCode("com.lawman.order.server.exception.order.ProductServiceCheckFailedException");
  }
}
