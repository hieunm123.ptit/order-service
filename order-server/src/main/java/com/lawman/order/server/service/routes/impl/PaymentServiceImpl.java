package com.lawman.order.server.service.routes.impl;

import com.lawman.order.client.dto.request.OrderRequest;
import com.lawman.order.client.dto.response.payment.PaymentResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.service.routes.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class PaymentServiceImpl implements PaymentService {
  PaymentResponse paymentResponseFake1 = new PaymentResponse(
        "paymentId",
        "ct01",
        5000d,
        null
  );

  PaymentResponse paymentResponseFake2 = new PaymentResponse(
        "paymentId2",
        "ct02",
        5000d,
        null
  );

  List<PaymentResponse> paymentResponsesFake = Arrays.asList(
        paymentResponseFake1,
        paymentResponseFake2
  );

  @Override
  public ResponseGeneral<String> createPayment(OrderRequest request) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, "paymentId");
  }

  @Override
  public ResponseGeneral<Void> revertPayment(OrderRequest request) {
    return null;
  }

  @Override
  public ResponseGeneral<PaymentResponse> detail(String paymentId) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, new PaymentResponse(
          "paymentId",
          "ct01",
          null,
          null
    ));
  }

  @Override
  public ResponseGeneral<List<PaymentResponse>> findByIds(List<String> paymentIds) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, paymentResponsesFake);
  }
}
