package com.lawman.order.server.service;

import com.lawman.order.client.dto.request.ProductOrderRequest;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.server.entity.ProductOrder;

import java.util.List;

public interface ProductOrderService {
  List<ProductOrder> createProductOrders(List<ProductOrderRequest> requests, String orderId);

  void saveProductOrders(List<ProductOrder> productOrders);

  String getIdByItemId(String productId, String productItemId, String orderId);

  List<ProductOrderDetailResponse> getByOrderId(String orderId);

  List<ProductOrderDetailResponse> getByIds(List<String> ids);

  Boolean checkExistByProductId(String productId);

  Boolean checkExistByProductItemId(String productItemId);

  ProductOrder getById(String id);
}
