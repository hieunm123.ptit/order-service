package com.lawman.order.server.service.routes;

import com.lawman.order.client.dto.request.ProductOrderRequest;
import com.lawman.order.client.dto.response.product.ProductItemResponse;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.client.dto.response.product.ProductResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.entity.ProductOrder;

import java.util.List;

public interface ProductService {
  ResponseGeneral<Void> deDuct(List<ProductOrderRequest> requests);

  ResponseGeneral<Void> revertCart(List<ProductOrderRequest> requests);

  ResponseGeneral<List<ProductResponse>> getProductByIds(List<String> ids);

  ResponseGeneral<List<ProductOrderDetailResponse>> details(List<ProductOrder> productOrders);

  ResponseGeneral<ProductItemResponse> detailProductItem(String productItemId);

  ResponseGeneral<List<ProductOrderDetailResponse>> findByKeyword(String keyword);
}
