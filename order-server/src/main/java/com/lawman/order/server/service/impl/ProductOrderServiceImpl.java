package com.lawman.order.server.service.impl;

import com.lawman.order.client.dto.request.ProductOrderRequest;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.server.entity.ProductOrder;
import com.lawman.order.server.exception.order.ProductOrderNotFoundException;
import com.lawman.order.server.repository.ProductOrderRepository;
import com.lawman.order.server.service.ProductOrderService;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
public class ProductOrderServiceImpl implements ProductOrderService {
  private final ProductOrderRepository productOrderRepository;

  public ProductOrderServiceImpl(ProductOrderRepository productOrderRepository) {
    this.productOrderRepository = productOrderRepository;
  }

  @Override
  public List<ProductOrder> createProductOrders(List<ProductOrderRequest> requests, String orderId) {
    log.info("(createProductOrders) requests: {}, orderId: {}", requests, orderId);

    List<ProductOrder> productOrders = new ArrayList<>();

    for (ProductOrderRequest request : requests) {
      ProductOrder productOrder = new ProductOrder(
            orderId,
            request.getProductId(),
            request.getProductItemId(),
            request.getQuantity(),
            request.getPrice(),
            request.getSellerId()
      );
      productOrders.add(productOrder);
    }

    return productOrderRepository.saveAll(productOrders);
  }

  @Override
  public void saveProductOrders(List<ProductOrder> productOrders) {
    log.info("(saveProductOrders) productOrders: {}", productOrders);

    productOrderRepository.saveAll(productOrders);
  }

  @Override
  public String getIdByItemId(String productId, String productItemId, String orderId) {
    log.info("(getIdByProductIdOrProductItemId) productId: {}, productItemId: {}", productId, productItemId);

    return productOrderRepository.getIdByProductIdOrProductItemIdAndOrderId(productId, productItemId,orderId);
  }

  @Override
  public List<ProductOrderDetailResponse> getByOrderId(String orderId) {
    log.info("(getByOrderId) orderId: {}", orderId);

    return productOrderRepository.getByOrderId(orderId);
  }

  @Override
  public List<ProductOrderDetailResponse> getByIds(List<String> ids) {
    log.info("(getByIds) ids: {}", ids);

    if (ids == null) {
      return new ArrayList<>();
    }

    return productOrderRepository.getByIds(ids);
  }

  @Override
  public Boolean checkExistByProductId(String productId) {
    log.info("(checkExistByProductId) productId: {}", productId);

    if (Objects.isNull(productId)) {
      return false;
    }

    return productOrderRepository.existsByProductId(productId);
  }

  @Override
  public Boolean checkExistByProductItemId(String productItemId) {
    log.info("(checkExistByProductItemId) productItemId: {}", productItemId);

    if (Objects.isNull(productItemId)) {
      return false;
    }

    return productOrderRepository.existsByProductItemId(productItemId);
  }

  @Override
  public ProductOrder getById(String id) {
    log.info("(getById) id: {}", id);

    return productOrderRepository.findById(id).orElseThrow(ProductOrderNotFoundException::new);
  }
}
