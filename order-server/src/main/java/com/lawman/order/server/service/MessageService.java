package com.lawman.order.server.service;

public interface MessageService {
  String getMessage(String code, String language);
}
