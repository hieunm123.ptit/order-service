package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.BadRequestException;

public class PaymentServiceCheckFailed extends BadRequestException {
  public PaymentServiceCheckFailed() {
    setCode("com.lawman.order.server.exception.order.PaymentServiceCheckFailed");
  }
}
