package com.lawman.order.server.facade;

import com.lawman.order.client.dto.request.OrderFacadeFilterRequest;
import com.lawman.order.client.dto.request.OrderRequest;
import com.lawman.order.client.dto.request.OrderUpdateRequest;
import com.lawman.order.client.dto.response.OrderDetailResponse;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.dto.response.common.PageResponse;

public interface OrderFacadeService {
  OrderResponse createOrder(OrderRequest request);

  OrderDetailResponse updateOrder(OrderUpdateRequest updateRequest, String orderId);

  OrderDetailResponse detailOrder(String orderId);

  PageResponse<OrderDetailResponse> filterOrder(OrderFacadeFilterRequest request);
}
