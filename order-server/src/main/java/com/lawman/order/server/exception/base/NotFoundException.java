package com.lawman.order.server.exception.base;


import static com.lawman.order.server.constanst.OrderConstants.StatusException.NOT_FOUND;

public class NotFoundException extends BaseException {
  public NotFoundException(String id, String objectName) {
    setCode("com.lawman.order.server.exception.base.NotFoundException");
    setStatus(NOT_FOUND);
    addParam("id", id);
    addParam("objectName", objectName);
  }

  public NotFoundException(){
    setCode("com.lawman.order.server.exception.base.NotFoundException");
    setStatus(NOT_FOUND);
  }
}
