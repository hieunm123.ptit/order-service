package com.lawman.order.server.service.routes;

import com.lawman.order.client.dto.request.AddressRequest;
import com.lawman.order.client.dto.request.OrderRequest;
import com.lawman.order.client.dto.request.ShipmentRequest;
import com.lawman.order.client.dto.response.shipment.ShipmentResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;

import java.util.List;

public interface ShipmentService {
  ResponseGeneral<String> createShipment(OrderRequest request);

  ResponseGeneral<ShipmentResponse> createShipmentOrigin(ShipmentRequest request);

  ResponseGeneral<String> updateAddress(AddressRequest request, String userId);

  ResponseGeneral<ShipmentResponse> detail(String shipmentId);

  ResponseGeneral<List<ShipmentResponse>> findByIds(List<String> ids);
}
