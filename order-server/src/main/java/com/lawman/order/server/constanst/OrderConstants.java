package com.lawman.order.server.constanst;

public class OrderConstants {
  private OrderConstants() {

  }

  public static class CommonConstants {
    private CommonConstants() {

    }
    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String LANGUAGE = "Accept-Language";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String MESSAGE_SOURCE = "classpath:i18n/messages";

    public static final String UNICODE = "Test_UniCode";
  }

  public static class StatusException {
    private StatusException() {

    }
    public static final Integer NOT_FOUND = 404;
    public static final Integer CONFLICT = 409;
    public static final Integer BAD_REQUEST = 400;
  }

  public static class MessageCode {
    private MessageCode() {

    }
    public static final String CREATE_ORDER_SUCCESS = "com.lawman.order.server.controller.create";
    public static final String UPDATE_ORDER_SUCCESS = "com.lawman.order.server.controller.update";
    public static final String DELETE_ORDER_SUCCESS = "com.lawman.order.server.controller.delete";

  }
}
