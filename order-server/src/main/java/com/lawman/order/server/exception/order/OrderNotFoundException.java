package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.NotFoundException;

public class OrderNotFoundException extends NotFoundException {
  public OrderNotFoundException() {
    setCode("com.lawman.order.server.exception.order.OrderNotFoundException");
  }
}
