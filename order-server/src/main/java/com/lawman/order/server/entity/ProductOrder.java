package com.lawman.order.server.entity;

import com.lawman.order.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "product_orders")
@Getter
@Setter
@AllArgsConstructor
public class ProductOrder extends BaseEntityWithUpdater {
  private String orderId;
  private String productId;
  private String productItemId;
  private Integer quantity;
  private Double price;
  private String sellerId;
  private Boolean isDeleted;

  public ProductOrder() {
    this.isDeleted = false;
  }

  public ProductOrder(String orderId,
                      String productId,
                      String productItemId,
                      Integer quantity,
                      Double price,
                      String sellerId
  ) {
    this.orderId = orderId;
    this.productId = productId;
    this.productItemId = productItemId;
    this.quantity = quantity;
    this.price = price;
    this.sellerId = sellerId;
    this.isDeleted = false;
  }
}
