package com.lawman.order.server.repository;

import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.server.entity.ProductOrder;
import com.lawman.order.server.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductOrderRepository extends BaseRepository<ProductOrder> {
  boolean existsByProductId(String productId);

  boolean existsByProductItemId(String productItemId);

  @Query(value = "select po.id from ProductOrder po " +
        "where (po.productId = :productId or po.productItemId = :productItemId) " +
        "and po.orderId = :orderId")
  String getIdByProductIdOrProductItemIdAndOrderId(String productId, String productItemId, String orderId);

  @Query(value = "select new com.lawman.order.client.dto.response.ProductOrderDetailResponse" +
        "(po.id, po.orderId, po.productId, po.productItemId, po.quantity, po.price, po.sellerId) " +
        "from ProductOrder po " +
        "where po.orderId = :orderId and po.isDeleted = false")
  List<ProductOrderDetailResponse> getByOrderId(String orderId);

  @Query(value = "select new com.lawman.order.client.dto.response.ProductOrderDetailResponse" +
        "(po.id, po.orderId, po.productId, po.productItemId, po.quantity, po.price, po.sellerId) " +
        "from ProductOrder po " +
        "where (po.id in :ids) and po.isDeleted = false")
  List<ProductOrderDetailResponse> getByIds(List<String> ids);

  Optional<ProductOrder> findById(String id);
}
