package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.BadRequestException;

public class ConnectServiceFailedException extends BadRequestException {
  public ConnectServiceFailedException() {
    setCode("com.lawman.order.server.exception.order.ConnectServiceFailedException");
  }
}
