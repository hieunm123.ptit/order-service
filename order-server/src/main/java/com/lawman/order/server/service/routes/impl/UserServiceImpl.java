package com.lawman.order.server.service.routes.impl;

import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.service.routes.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class UserServiceImpl implements UserService {
  @Override
  public ResponseGeneral<Void> checkUserExists(String id) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, null);
  }

  @Override
  public ResponseGeneral<List<String>> findIdsByKeyword(String keyword) {
    return ResponseGeneral.of(
          HttpStatus.OK.value(),
          null,
          Arrays.asList("sl01", "sl02", "sl05"));  }
}
