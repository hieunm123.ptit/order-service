package com.lawman.order.server.service;

import com.lawman.order.client.dto.request.OrderFilterRequest;
import com.lawman.order.client.dto.request.OrderStatusRequest;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.dto.response.common.PageResponse;
import com.lawman.order.server.entity.Order;
import com.lawman.order.server.service.base.BaseService;

public interface OrderService extends BaseService<Order> {
  OrderResponse createOrder(Order order);

  OrderResponse updateShipmentId(String orderId, String shipmentId);

  OrderResponse detail(String id);

  void remove(String id);

  void updateOrderStatus(String id, OrderStatusRequest request);

  PageResponse<OrderResponse> filter(OrderFilterRequest request);
}
