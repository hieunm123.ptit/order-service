package com.lawman.order.server.configuration;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.lawman.order.server")
public class JpaRepositoryConfiguration {
}
