package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.BadRequestException;

public class CanNotUpdateOrderException extends BadRequestException {
  public CanNotUpdateOrderException() {
    setCode("com.lawman.order.server.exception.order.CanNotUpdateOrderException");
  }
}
