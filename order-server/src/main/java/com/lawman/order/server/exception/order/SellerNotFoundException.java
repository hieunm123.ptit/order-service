package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.NotFoundException;

public class SellerNotFoundException extends NotFoundException {
  public SellerNotFoundException() {
    setCode("com.lawman.order.server.exception.order.SellerNotFoundException");
  }
}
