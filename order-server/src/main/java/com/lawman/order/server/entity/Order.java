package com.lawman.order.server.entity;

import com.lawman.order.client.enums.OrderStatus;
import com.lawman.order.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "orders")
@Getter
@Setter
@AllArgsConstructor
public class Order extends BaseEntityWithUpdater {
  private String customerId;
  private Double totalAmount;
  private String code;
  private String shipmentId;
  private String paymentId;
  @Enumerated(EnumType.ORDINAL)
  private OrderStatus status;
  private Boolean isDeleted;

  public Order() {
    this.status = OrderStatus.ORDER_WAITING;
    this.isDeleted = false;
  }
}
