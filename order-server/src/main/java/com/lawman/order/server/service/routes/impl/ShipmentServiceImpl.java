package com.lawman.order.server.service.routes.impl;

import com.lawman.order.client.dto.request.AddressRequest;
import com.lawman.order.client.dto.request.OrderRequest;
import com.lawman.order.client.dto.request.ShipmentRequest;
import com.lawman.order.client.dto.response.shipment.ShipmentResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.service.routes.ShipmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ShipmentServiceImpl implements ShipmentService {
  @Override
  public ResponseGeneral<String> createShipment(OrderRequest request) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, "shipmentId");
  }

  @Override
  public ResponseGeneral<ShipmentResponse> createShipmentOrigin(ShipmentRequest request) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, new ShipmentResponse(
          "shipmentId",
          null,
          null,
          null,
          null,
          null,
          null
    ));
  }

  @Override
  public ResponseGeneral<String> updateAddress(AddressRequest request, String userId) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, "addressId");
  }

  @Override
  public ResponseGeneral<ShipmentResponse> detail(String shipmentId) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, new ShipmentResponse());
  }

  @Override
  public ResponseGeneral<List<ShipmentResponse>> findByIds(List<String> ids) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, new ArrayList<>());
  }
}
