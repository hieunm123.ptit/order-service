package com.lawman.order.server.repository;

import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.enums.OrderStatus;
import com.lawman.order.server.entity.Order;
import com.lawman.order.server.repository.base.BaseRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends BaseRepository<Order> {
  @Query(value = "update Order set shipmentId = :shipmentId where id = :orderId")
  @Modifying
  void updateShipmentId(String orderId, String shipmentId);

  Optional<Order> findById(String id);

  boolean existsById(String id);

  @Query(value = "update Order set isDeleted = true where id = :id")
  @Modifying
  void removeById(String id);

  @Query(value = "update Order set status = :status where id = :id")
  @Modifying
  void updateOrderStatus(String id, OrderStatus status);

  @Query(value = "select distinct new com.lawman.order.client.dto.response.OrderResponse(" +
        "o.id, o.customerId, o.totalAmount, o.code, o.shipmentId, o.paymentId, o.status) " +
        "from Order o join ProductOrder po on o.id = po.orderId " +
        "where coalesce(:productIds) is null or po.productId in :productIds " +
        "and coalesce(:sellerIds) is null or po.sellerId in :sellerIds " +
        "and (:customerId is null or o.customerId = :customerId) " +
        "and (:status is null or o.status = :status) " +
        "and (:from is null or :to is null or (o.createdAt between :from and :to)) " +
        "and (o.isDeleted = false)")
  List<OrderResponse> filter(
        List<String> productIds,
        List<String> sellerIds,
        String customerId,
        OrderStatus status,
        Long from,
        Long to,
        Pageable pageable
  );

  @Query(value = "select count (o)" +
        "from Order o join ProductOrder po on o.id = po.orderId " +
        "where coalesce(:productIds) is null or po.productId in :productIds " +
        "and coalesce(:sellerIds) is null or po.sellerId in :sellerIds " +
        "and (:customerId is null or o.customerId = :customerId) " +
        "and (:status is null or o.status = :status) " +
        "and (:from is null or :to is null or (o.createdAt between :from and :to)) " +
        "and (o.isDeleted = false)")
  int countFilter(
        List<String> productIds,
        List<String> sellerIds,
        String customerId,
        OrderStatus status,
        Long from,
        Long to
  );
}
