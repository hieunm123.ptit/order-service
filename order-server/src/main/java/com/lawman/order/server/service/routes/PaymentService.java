package com.lawman.order.server.service.routes;

import com.lawman.order.client.dto.request.OrderRequest;
import com.lawman.order.client.dto.response.payment.PaymentResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;

import java.util.List;

public interface PaymentService {
  ResponseGeneral<String> createPayment(OrderRequest request);

  ResponseGeneral<Void> revertPayment(OrderRequest request);

  ResponseGeneral<PaymentResponse> detail(String paymentId);

  ResponseGeneral<List<PaymentResponse>> findByIds(List<String> paymentIds);
}
