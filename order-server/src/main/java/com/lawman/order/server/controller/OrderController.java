package com.lawman.order.server.controller;

import com.lawman.order.client.dto.request.*;
import com.lawman.order.client.dto.response.OrderDetailResponse;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.dto.response.common.PageResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.facade.OrderFacadeService;
import com.lawman.order.server.service.MessageService;
import com.lawman.order.server.service.OrderService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.lawman.order.server.constanst.OrderConstants.CommonConstants.DEFAULT_LANGUAGE;
import static com.lawman.order.server.constanst.OrderConstants.CommonConstants.LANGUAGE;
import static com.lawman.order.server.constanst.OrderConstants.MessageCode.*;

@RestController
@RequestMapping("/api/v1/orders")
@ResponseBody
@RequiredArgsConstructor
@Slf4j
public class OrderController {
  private final MessageService messageService;
  private final OrderFacadeService orderFacadeService;
  private final OrderService orderService;

  @PostMapping
  public ResponseGeneral<OrderResponse> create(
        @RequestBody @Valid OrderRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(create) request: {}", request);

    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_ORDER_SUCCESS, language),
          orderFacadeService.createOrder(request));
  }

  @PutMapping("{id}")
  public ResponseGeneral<OrderDetailResponse> update(
        @PathVariable String id,
        @RequestBody @Valid OrderUpdateRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(update) id: {} updateRequest: {}", id, request);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(UPDATE_ORDER_SUCCESS, language),
          orderFacadeService.updateOrder(request, id));
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(delete) id: {}", id);

    orderService.remove(id);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(DELETE_ORDER_SUCCESS, language)
    );
  }

  @PutMapping("/{id}/order-status")
  public ResponseGeneral<Void> updateOrderStatus(
        @PathVariable String id,
        @Valid @RequestBody OrderStatusRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(update) id: {}, request: {}", id, request);

    orderService.updateOrderStatus(id, request);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(UPDATE_ORDER_SUCCESS, language));
  }

  @PostMapping("/filter")
  public ResponseGeneral<PageResponse<OrderDetailResponse>> filter(
        @RequestBody OrderFacadeFilterRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(filter) request: {}", request);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(CREATE_ORDER_SUCCESS, language),
          orderFacadeService.filterOrder(request)
    );
  }
}
