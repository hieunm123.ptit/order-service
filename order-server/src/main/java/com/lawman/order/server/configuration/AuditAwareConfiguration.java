//package com.lawman.order.server.configuration;
//
//
//import com.lawman.order.server.configuration.auditor.AuditorAwareImpl;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.domain.AuditorAware;
//import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
//
//@Configuration
//@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
//public class AuditAwareConfiguration {
//  @Bean
//  public AuditorAware<String> auditorProvider() {return new AuditorAwareImpl();}
//}
