package com.lawman.order.server.service.routes;


import com.lawman.order.server.dto.response.ResponseGeneral;

import java.util.List;

public interface UserService {
  ResponseGeneral<Void> checkUserExists(String id);
  ResponseGeneral<List<String>> findIdsByKeyword(String keyword);
}
