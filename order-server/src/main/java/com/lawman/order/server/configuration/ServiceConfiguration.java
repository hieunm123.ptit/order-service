package com.lawman.order.server.configuration;

import com.lawman.order.server.facade.OrderFacadeService;
import com.lawman.order.server.facade.impl.OrderFacadeServiceImpl;
import com.lawman.order.server.repository.OrderRepository;
import com.lawman.order.server.repository.ProductOrderRepository;
import com.lawman.order.server.service.MessageService;
import com.lawman.order.server.service.OrderService;
import com.lawman.order.server.service.ProductOrderService;
import com.lawman.order.server.service.impl.MessageServiceImpl;
import com.lawman.order.server.service.impl.OrderServiceImpl;
import com.lawman.order.server.service.impl.ProductOrderServiceImpl;
import com.lawman.order.server.service.routes.PaymentService;
import com.lawman.order.server.service.routes.ProductService;
import com.lawman.order.server.service.routes.ShipmentService;
import com.lawman.order.server.service.routes.UserService;
import com.lawman.order.server.service.routes.impl.PaymentServiceImpl;
import com.lawman.order.server.service.routes.impl.ProductServiceImpl;
import com.lawman.order.server.service.routes.impl.ShipmentServiceImpl;
import com.lawman.order.server.service.routes.impl.UserServiceImpl;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {
  @Bean
  public MessageService messageService(MessageSource messageSource) {
    return new MessageServiceImpl(messageSource);
  }

  @Bean
  OrderService orderService(OrderRepository repository) {
    return new OrderServiceImpl(repository);
  }

  @Bean
  ProductOrderService productOrderService(ProductOrderRepository productOrderRepository) {
    return new ProductOrderServiceImpl(productOrderRepository);
  }

  @Bean
  UserService userService() {
    return new UserServiceImpl();
  }

  @Bean
  ProductService productService() {
    return new ProductServiceImpl();
  }

  @Bean
  PaymentService paymentService() {
    return new PaymentServiceImpl();
  }

  @Bean
  ShipmentService shipmentService() {
    return new ShipmentServiceImpl();
  }

  @Bean
  OrderFacadeService orderFacadeService(
        OrderService orderService,
        ProductOrderService productOrderService,
        UserService userService,
        ProductService productService,
        PaymentService paymentService,
        ShipmentService shipmentService) {
    return new OrderFacadeServiceImpl(
          orderService,
          productOrderService,
          userService,
          productService,
          paymentService,
          shipmentService);
  }
}
