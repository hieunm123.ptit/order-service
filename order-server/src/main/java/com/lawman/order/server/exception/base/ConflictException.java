package com.lawman.order.server.exception.base;


import static com.lawman.order.server.constanst.OrderConstants.StatusException.CONFLICT;

public class ConflictException extends BaseException {
  public ConflictException(String id, String objectName) {
    setStatus(CONFLICT);
    setCode("com.lawman.order.server.exception.base.ConflictException");
    addParam("id", id);
    addParam("objectName", objectName);
  }

  public ConflictException(){
    setStatus(CONFLICT);
    setCode("com.lawman.order.server.exception.base.ConflictException");
  }
}
