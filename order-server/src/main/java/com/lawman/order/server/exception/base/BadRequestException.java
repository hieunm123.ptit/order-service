package com.lawman.order.server.exception.base;


import static com.lawman.order.server.constanst.OrderConstants.StatusException.BAD_REQUEST;

public class BadRequestException extends BaseException {
  public BadRequestException() {
    setCode("com.lawman.order.server.exception.base.BadRequestException");
    setStatus(BAD_REQUEST);
  }
}
