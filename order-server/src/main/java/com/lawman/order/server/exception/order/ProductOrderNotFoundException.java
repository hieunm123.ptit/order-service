package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.NotFoundException;

public class ProductOrderNotFoundException extends NotFoundException {
  public ProductOrderNotFoundException() {
    setCode("com.lawman.order.server.exception.order.ProductOrderNotFoundException");
  }
}
