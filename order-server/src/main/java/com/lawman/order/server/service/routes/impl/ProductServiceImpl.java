package com.lawman.order.server.service.routes.impl;

import com.lawman.order.client.dto.request.ProductOrderRequest;
import com.lawman.order.client.dto.response.product.ProductItemResponse;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.client.dto.response.product.ProductResponse;
import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.entity.ProductOrder;
import com.lawman.order.server.service.routes.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ProductServiceImpl implements ProductService {
  @Override
  public ResponseGeneral<Void> deDuct(List<ProductOrderRequest> requests) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, null);
  }

  @Override
  public ResponseGeneral<Void> revertCart(List<ProductOrderRequest> requests) {
    return null;
  }

  @Override
  public ResponseGeneral<List<ProductResponse>> getProductByIds(List<String> ids) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, new ArrayList<>());
  }

  @Override
  public ResponseGeneral<List<ProductOrderDetailResponse>> details(List<ProductOrder> productOrders) {
    return ResponseGeneral.of(HttpStatus.OK.value(), null, new ArrayList<>());
  }

  @Override
  public ResponseGeneral<ProductItemResponse> detailProductItem(String productItemId) {
    return ResponseGeneral.of(
          HttpStatus.OK.value(),
          null,
          new ProductItemResponse(null, 200.0, 300.0, 3));
  }

  @Override
  public ResponseGeneral<List<ProductOrderDetailResponse>> findByKeyword(String keyword) {
    return ResponseGeneral.of(
          HttpStatus.OK.value(),
          null,
          new ArrayList<>());
  }
}
