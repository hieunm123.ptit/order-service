package com.lawman.order.server.exception.order;

import com.lawman.order.server.exception.base.BadRequestException;

public class ShipmentServiceCheckFailed extends BadRequestException {
  public ShipmentServiceCheckFailed() {
    setCode("com.lawman.order.server.exception.order.ShipmentServiceCheckFailed");
  }
}
