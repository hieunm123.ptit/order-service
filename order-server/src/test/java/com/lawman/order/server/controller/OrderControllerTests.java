package com.lawman.order.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lawman.order.client.dto.request.*;
import com.lawman.order.client.dto.response.OrderDetailResponse;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.client.dto.response.payment.PaymentResponse;
import com.lawman.order.client.dto.response.shipment.ShipmentResponse;
import com.lawman.order.server.config.ConfigTest;
import com.lawman.order.server.exception.order.OrderNotFoundException;
import com.lawman.order.server.facade.OrderFacadeService;
import com.lawman.order.server.service.MessageService;
import com.lawman.order.server.service.OrderService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static com.lawman.order.server.constanst.OrderConstants.MessageCode.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(OrderController.class)
@ContextConfiguration(classes = {
      ConfigTest.class
})
class OrderControllerTests {
  private static final String END_POINT_PATH = "/api/v1/orders";

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  OrderController orderController;

  @MockBean
  MessageService messageService;

  @MockBean
  OrderFacadeService orderFacadeService;

  @MockBean
  OrderService orderService;

  static ProductOrderRequest productOrderRequestMock1 = new ProductOrderRequest(
        "01",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrderRequest productOrderRequestMock2 = new ProductOrderRequest(
        "02",
        null,
        3,
        600.0,
        "sl02"
  );

  static List<ProductOrderRequest> productOrderRequestsMock = Arrays.asList(
        productOrderRequestMock1,
        productOrderRequestMock2
  );

  static OrderRequest orderRequestMock = new OrderRequest(
        "ct01",
        productOrderRequestsMock,
        null,
        null,
        null
  );

  static OrderResponse orderResponseMock = new OrderResponse(
        "001",
        "ct01",
        null,
        2800.0,
        null,
        "shipmentId",
        "paymentId",
        0
  );

  static AddressRequest fromAddressRequestMock = new AddressRequest(
        "01",
        "001",
        "0001",
        ""
  );

  static AddressRequest toAddressRequestMock = new AddressRequest(
        "02",
        "002",
        "0002",
        ""
  );

  static OrderUpdateRequest orderUpdateRequest = new OrderUpdateRequest(
        "ct01",
        productOrderRequestsMock,
        fromAddressRequestMock,
        toAddressRequestMock,
        null,
        null
  );

  static ProductOrderDetailResponse productOrderDetailResponseMock1 = new ProductOrderDetailResponse(
        "0001",
        "001",
        "01",
        null,
        "Product1",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrderDetailResponse productOrderDetailResponseMock2 = new ProductOrderDetailResponse(
        "0002",
        "001",
        "02",
        null,
        "Product2",
        null,
        3,
        600.0,
        "sl02"
  );

  static List<ProductOrderDetailResponse> productOrderDetailResponsesMock = Arrays.asList(
        productOrderDetailResponseMock1,
        productOrderDetailResponseMock2);

  static ShipmentResponse shipmentResponseMock = new ShipmentResponse(
        "shipmentId",
        null,
        null,
        null,
        null,
        null,
        null
  );

  static PaymentResponse paymentResponseMock = new PaymentResponse(
        "paymentId",
        "ct01",
        null,
        null
  );

  static OrderDetailResponse orderDetailResponse = new OrderDetailResponse(
        "001",
        "ct01",
        productOrderDetailResponsesMock,
        2800.0,
        null,
        shipmentResponseMock,
        paymentResponseMock,
        0
  );

  @Test
  void create_WhenInputValid_Return201CREATEDAndResponse() throws Exception {
    OrderRequest request = orderRequestMock;
    OrderResponse response = orderResponseMock;

    Mockito.when(orderFacadeService.createOrder(request)).thenReturn(response);
    Mockito.when(messageService.getMessage(CREATE_ORDER_SUCCESS, "en"))
          .thenReturn(" Create order successfully");

    MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                      .post(END_POINT_PATH)
                      .contentType("application/json").content(objectMapper.writeValueAsString(request)))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(jsonPath("$.status").value("201"))
          .andDo(MockMvcResultHandlers.print())
          .andReturn();

    String actual = mvcResult.getResponse().getContentAsString();
    String expect = objectMapper.writeValueAsString(orderController.create(request, "en"));

    Assertions.assertThat(actual).isEqualTo(expect);
  }

  @Test
  void create_WhenCustomerIdIsNull_Return400BADREQUEST() throws Exception {
    OrderRequest request = new OrderRequest();
    request.setProductOrders(productOrderRequestsMock);

    Mockito.when(messageService.getMessage(CREATE_ORDER_SUCCESS, "en"))
          .thenReturn(" Create order successfully");

    mockMvc.perform(
                MockMvcRequestBuilders
                      .post(END_POINT_PATH)
                      .contentType("application/json").content(objectMapper.writeValueAsString(request)))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value("com.lawman.order.client.dto.request.customerId.NotNull"))
          .andDo(MockMvcResultHandlers.print());
  }

  @Test
  void create_WhenProductOrdersIsNull_Return400BADREQUEST() throws Exception {
    OrderRequest request = new OrderRequest();
    request.setCustomerId("ct01");

    Mockito.when(messageService.getMessage(CREATE_ORDER_SUCCESS, "en"))
          .thenReturn(" Create order successfully");

    mockMvc.perform(
                MockMvcRequestBuilders
                      .post(END_POINT_PATH)
                      .contentType("application/json").content(objectMapper.writeValueAsString(request)))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value("com.lawman.order.client.dto.request.productOrders.NotNull"))
          .andDo(MockMvcResultHandlers.print());
  }

  @Test
  void update_WhenInputValid_Return200OKAndResponse() throws Exception {
    OrderUpdateRequest updateRequest = orderUpdateRequest;
    String id = "001";
    OrderDetailResponse detailResponse = orderDetailResponse;

    Mockito.when(orderFacadeService.updateOrder(updateRequest, id)).thenReturn(detailResponse);
    Mockito.when(messageService.getMessage(UPDATE_ORDER_SUCCESS, "en"))
          .thenReturn(" Update order successfully");

    MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                      .put(END_POINT_PATH + "/" + id)
                      .contentType("application/json").content(objectMapper.writeValueAsString(updateRequest)))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(jsonPath("$.status").value("200"))
          .andDo(MockMvcResultHandlers.print())
          .andReturn();

    String actual = mvcResult.getResponse().getContentAsString();
    String expect = objectMapper.writeValueAsString(orderController.update(id, updateRequest, "en"));

    Assertions.assertThat(actual).isEqualTo(expect);
  }

  @Test
  void delete_WhenInputValid_Return200OK() throws Exception {
    String id = "001";

    Mockito.when(messageService.getMessage(DELETE_ORDER_SUCCESS, "en"))
          .thenReturn(" Delete order successfully");

    mockMvc.perform(
                MockMvcRequestBuilders
                      .delete(END_POINT_PATH + "/" + id)
                      .contentType("application/json"))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(jsonPath("$.status").value("200"))
          .andDo(MockMvcResultHandlers.print());
  }

  @Test
  void delete_WhenIdInValid_Return404NotFound() throws Exception {
    String id = "005";

    Mockito.when(messageService.getMessage(DELETE_ORDER_SUCCESS, "en"))
          .thenReturn(" Delete order successfully");
    Mockito.doThrow(new OrderNotFoundException()).when(orderService).remove(id);

    mockMvc.perform(
                MockMvcRequestBuilders
                      .delete(END_POINT_PATH + "/" + id)
                      .contentType("application/json"))
          .andExpect(MockMvcResultMatchers.status().isNotFound())
          .andExpect(jsonPath("$.status").value("404"))
          .andExpect(jsonPath("$.data.code")
                .value("com.lawman.order.server.exception.order.OrderNotFoundException"))
          .andDo(MockMvcResultHandlers.print());
  }

  @Test
  void updateOrderStatus_WhenStatusInvalid_Return400BadRequest() throws Exception {
    String id = "001";
    OrderStatusRequest statusRequest = new OrderStatusRequest(5);

    Mockito.when(messageService.getMessage(UPDATE_ORDER_SUCCESS, "en"))
          .thenReturn("Update order successfully");

    mockMvc.perform(
                MockMvcRequestBuilders
                      .put(END_POINT_PATH + "/" + id + "/order-status")
                      .contentType("application/json").content(objectMapper.writeValueAsString(statusRequest)))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.status").value("400"))
          .andExpect(jsonPath("$.data.detail").value("must be less than or equal to 3"))
          .andDo(MockMvcResultHandlers.print());
  }

  @Test
  void updateOrderStatus_WhenStatusInvalidAndIdDoesNotExisted_Return404NotFound() throws Exception {
    String id = "005";
    OrderStatusRequest statusRequest = new OrderStatusRequest(3);

    Mockito.when(messageService.getMessage(UPDATE_ORDER_SUCCESS, "en"))
          .thenReturn("Update order successfully");
    Mockito.doThrow(new OrderNotFoundException()).when(orderService).updateOrderStatus(id, statusRequest);

    mockMvc.perform(
                MockMvcRequestBuilders
                      .put(END_POINT_PATH + "/" + id + "/order-status")
                      .contentType("application/json").content(objectMapper.writeValueAsString(statusRequest)))
          .andExpect(MockMvcResultMatchers.status().isNotFound())
          .andExpect(jsonPath("$.status").value("404"))
          .andExpect(jsonPath("$.data.code").value("com.lawman.order.server.exception.order.OrderNotFoundException"))
          .andDo(MockMvcResultHandlers.print());
  }

  @Test
  void updateOrderStatus_WhenInputValid_Return200OK() throws Exception {
    String id = "001";
    OrderStatusRequest statusRequest = new OrderStatusRequest(2);

    Mockito.when(messageService.getMessage(UPDATE_ORDER_SUCCESS, "en"))
          .thenReturn("Update order successfully");

    mockMvc.perform(
                MockMvcRequestBuilders
                      .put(END_POINT_PATH + "/" + id + "/order-status")
                      .contentType("application/json").content(objectMapper.writeValueAsString(statusRequest)))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(jsonPath("$.status").value("200"))
          .andExpect(jsonPath("$.message").value("Update order successfully"))
          .andDo(MockMvcResultHandlers.print());
  }
}
