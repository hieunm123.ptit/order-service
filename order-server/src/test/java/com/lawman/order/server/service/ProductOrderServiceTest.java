package com.lawman.order.server.service;

import com.lawman.order.client.dto.request.ProductOrderRequest;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.server.config.ConfigTest;
import com.lawman.order.server.entity.ProductOrder;
import com.lawman.order.server.repository.ProductOrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@WebMvcTest(ProductOrderService.class)
@ContextConfiguration(classes = {
      ConfigTest.class
})
class ProductOrderServiceTest {
  @MockBean
  ProductOrderRepository productOrderRepository;

  @Autowired
  ProductOrderService productOrderService;

  static ProductOrderRequest productOrderRequestMock1 = new ProductOrderRequest(
        "01",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrderRequest productOrderRequestMock2 = new ProductOrderRequest(
        "02",
        null,
        3,
        600.0,
        "sl02"
  );

  static List<ProductOrderRequest> productOrderRequestsMock = Arrays.asList(
        productOrderRequestMock1,
        productOrderRequestMock2
  );

  static ProductOrder productOrderMock1 = new ProductOrder(
        "001",
        "01",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrder productOrderMock2 = new ProductOrder(
        "001",
        "02",
        null,
        3,
        600.0,
        "sl02"
  );

  static ProductOrderDetailResponse productOrderDetailResponseMock1 = new ProductOrderDetailResponse(
        null,
        "001",
        "01",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrderDetailResponse productOrderDetailResponseMock2 = new ProductOrderDetailResponse(
        null,
        "001",
        "02",
        null,
        3,
        600.0,
        "sl02"
  );

  static List<ProductOrderDetailResponse> productOrderDetailResponsesMock = Arrays.asList(
        productOrderDetailResponseMock1,
        productOrderDetailResponseMock2
  );

  static List<ProductOrder> productOrdersMock = Arrays.asList(productOrderMock1, productOrderMock2);

  @Test
  void createProductOrders_whenInputValid_returnListProductOrders() {
    List<ProductOrderRequest> productOrderRequests = productOrderRequestsMock;
    String orderId = "001";

    when(productOrderRepository.saveAll(anyList())).thenReturn(productOrdersMock);

    List<ProductOrder> productOrders = productOrderService.createProductOrders(productOrderRequests, orderId);

    assertEquals(productOrders, productOrdersMock);
  }

  @Test
  void saveProductOrders_whenInputValid_shouldSavaAll() {
    List<ProductOrder> productOrders = productOrdersMock;

    productOrderService.saveProductOrders(productOrders);

    verify(productOrderRepository, times(1)).saveAll(productOrders);
  }

  @Test
  void getIdByProductIdOrProductItemIdAndOrderId_whenInputValid_returnProductOrderId() {
    String productOrderIdMock = "0001";
    String productId = "001";
    String productItemId = "002";
    String orderId = "01";

    when(productOrderRepository.getIdByProductIdOrProductItemIdAndOrderId(
          productId,
          productItemId,
          orderId))
          .thenReturn(productOrderIdMock);

    String productOrderId = productOrderService.getIdByItemId(
          productId,
          productItemId,
          orderId
    );

    assertEquals(productOrderIdMock, productOrderId);
  }

  @Test
  void getIdByProductIdOrProductItemIdAndOrderId_whenInputInValid_returnProductOrderId() {
    String productId = "001";
    String productItemId = "002";
    String orderId = "01";

    when(productOrderRepository.getIdByProductIdOrProductItemIdAndOrderId(
          productId,
          productItemId,
          orderId))
          .thenReturn(null);

    String productOrderId = productOrderService.getIdByItemId(
          productId,
          productItemId,
          orderId
    );

    assertNull(productOrderId);
  }

  @Test
  void getByOrderId_whenInputValid_returnProductOrderDetailResponses() {
    String orderId = "01";

    when(productOrderRepository.getByOrderId(orderId)).thenReturn(productOrderDetailResponsesMock);

    List<ProductOrderDetailResponse> responses = productOrderService.getByOrderId(orderId);

    assertEquals(responses, productOrderDetailResponsesMock);
  }

  @Test
  void checkExistByProductId_whenProductIdNotNullAndValid_returnTrue() {
    String productId = "001";

    when(productOrderRepository.existsByProductId(productId)).thenReturn(true);

    Boolean checkExist = productOrderService.checkExistByProductId(productId);

    assertEquals(true, checkExist);
  }

  @Test
  void checkExistByProductId_whenProductIdNotNullAndInValid_returnFalse() {
    String productId = "005";

    when(productOrderRepository.existsByProductId(productId)).thenReturn(false);

    Boolean checkExist = productOrderService.checkExistByProductId(productId);

    assertEquals(false, checkExist);
  }

  @Test
  void checkExistByProductId_whenProductIdIsNull_returnFalse() {
    Boolean checkExist = productOrderService.checkExistByProductId(null);

    assertEquals(false, checkExist);
  }

  @Test
  void checkExistByProductItemId_whenProductItemIdNotNullAndValid_returnTrue() {
    String productItemId = "001";

    when(productOrderRepository.existsByProductItemId(productItemId)).thenReturn(true);

    Boolean checkExist = productOrderService.checkExistByProductItemId(productItemId);

    assertEquals(true, checkExist);
  }

  @Test
  void checkExistByProductItemId_whenProductItemIdNotNullAndInValid_returnFalse() {
    String productItemId = "005";

    when(productOrderRepository.existsByProductItemId(productItemId)).thenReturn(false);

    Boolean checkExist = productOrderService.checkExistByProductItemId(productItemId);

    assertEquals(false, checkExist);
  }

  @Test
  void checkExistByProductItemId_whenProductItemIdIsNull_returnFalse() {
    Boolean checkExist = productOrderService.checkExistByProductItemId(null);

    assertEquals(false, checkExist);
  }
}
