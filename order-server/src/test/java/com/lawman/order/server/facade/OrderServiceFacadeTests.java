package com.lawman.order.server.facade;

import com.lawman.order.client.dto.request.AddressRequest;
import com.lawman.order.client.dto.request.OrderRequest;
import com.lawman.order.client.dto.request.OrderUpdateRequest;
import com.lawman.order.client.dto.request.ProductOrderRequest;
import com.lawman.order.client.dto.response.OrderDetailResponse;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.dto.response.ProductOrderDetailResponse;
import com.lawman.order.client.dto.response.payment.PaymentResponse;
import com.lawman.order.client.dto.response.product.ProductResponse;
import com.lawman.order.client.dto.response.shipment.AddressResponse;
import com.lawman.order.client.dto.response.shipment.ShipmentResponse;
import com.lawman.order.server.config.ConfigTest;
import com.lawman.order.server.dto.response.ResponseGeneral;
import com.lawman.order.server.entity.Order;
import com.lawman.order.server.entity.ProductOrder;
import com.lawman.order.server.service.OrderService;
import com.lawman.order.server.service.ProductOrderService;
import com.lawman.order.server.service.routes.PaymentService;
import com.lawman.order.server.service.routes.ProductService;
import com.lawman.order.server.service.routes.ShipmentService;
import com.lawman.order.server.service.routes.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


import java.util.Arrays;
import java.util.List;

@WebMvcTest(OrderFacadeService.class)
@ContextConfiguration(classes = {
      ConfigTest.class
})
class OrderServiceFacadeTests {
  @Autowired
  OrderFacadeService service;

  @MockBean
  OrderService orderService;

  @MockBean
  ProductOrderService productOrderService;

  @MockBean
  UserService userService;

  @MockBean
  ProductService productService;

  @MockBean
  PaymentService paymentService;

  @MockBean
  ShipmentService shipmentService;

  static ProductOrderRequest productOrderRequestMock1 = new ProductOrderRequest(
        "01",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrderRequest productOrderRequestMock2 = new ProductOrderRequest(
        "02",
        null,
        3,
        600.0,
        "sl02"
  );

  static List<ProductOrderRequest> productOrderRequestsMock = Arrays.asList(
        productOrderRequestMock1,
        productOrderRequestMock2
  );

  static ProductResponse productResponseMock1 = new ProductResponse(
        "01",
        null,
        "Product1",
        null
  );

  static ProductResponse productResponseMock2 = new ProductResponse(
        "02",
        null,
        "Product2",
        null
  );

  static ProductOrderDetailResponse productOrderDetailResponseMock1 = new ProductOrderDetailResponse(
        "0001",
        "001",
        "01",
        null,
        "Product1",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrderDetailResponse productOrderDetailResponseMock2 = new ProductOrderDetailResponse(
        "0002",
        "001",
        "02",
        null,
        "Product2",
        null,
        3,
        600.0,
        "sl02"
  );

  static List<ProductOrderDetailResponse> productOrderDetailResponsesMock = Arrays.asList(
        productOrderDetailResponseMock1,
        productOrderDetailResponseMock2);

  static ProductOrder productOrderMock1 = new ProductOrder(
        "001",
        "01",
        null,
        2,
        500.0,
        "sl01"
  );

  static ProductOrder productOrderMock2 = new ProductOrder(
        "001",
        "02",
        null,
        3,
        600.0,
        "sl02"
  );

  static List<ProductOrder> productOrdersMock = Arrays.asList(productOrderMock1, productOrderMock2);

  static OrderRequest orderRequestMock = new OrderRequest(
        "ct01",
        productOrderRequestsMock,
        null,
        null,
        null
  );

  static AddressRequest fromAddressRequestMock = new AddressRequest(
        "01",
        "001",
        "0001",
        ""
  );

  static AddressRequest toAddressRequestMock = new AddressRequest(
        "02",
        "002",
        "0002",
        ""
  );

  static OrderUpdateRequest orderUpdateRequest = new OrderUpdateRequest(
        "ct01",
        productOrderRequestsMock,
        fromAddressRequestMock,
        toAddressRequestMock,
        null,
        null
  );

  static AddressResponse fromAddressResponseMock = new AddressResponse();

  static AddressResponse toAddressResponseMock = new AddressResponse();

  static ShipmentResponse shipmentResponseMock = new ShipmentResponse(
        "shipmentId",
        null,
        null,
        null,
        null,
        null,
        null
  );

  static PaymentResponse paymentResponseMock = new PaymentResponse(
        "paymentId",
        "ct01",
        null,
        null
  );

  static OrderDetailResponse orderDetailResponse = new OrderDetailResponse(
        "001",
        "ct01",
        productOrderDetailResponsesMock,
        2800.0,
        null,
        shipmentResponseMock,
        paymentResponseMock,
        0
  );

  OrderResponse orderResponseMock = new OrderResponse(
        "001",
        "ct01",
        null,
        2800.0,
        null,
        "shipmentId",
        "paymentId",
        0
  );

  @Test
  void createOrder_WhenInputValid_ReturnOrderResponse() {
    OrderRequest orderRequest = orderRequestMock;

    when(userService.checkUserExists(orderRequest.getCustomerId())).thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, null));
    when(productService.deDuct(orderRequest.getProductOrders())).thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, null));
    when(paymentService.createPayment(orderRequest)).thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, "paymentId"));
    when(shipmentService.createShipment(orderRequest)).thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, "shipmentId"));
    when(orderService.createOrder(any(Order.class))).thenReturn(orderResponseMock);
    when(productOrderService.createProductOrders(orderRequest.getProductOrders(), orderResponseMock.getId())).thenReturn(productOrdersMock);
    when(productService.details(productOrdersMock)).thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, productOrderDetailResponsesMock));

    OrderResponse orderResponse = service.createOrder(orderRequest);

    verify(userService, times(1)).checkUserExists(orderRequest.getCustomerId());
    verify(productService, times(1)).deDuct(orderRequest.getProductOrders());
    verify(paymentService, times(1)).createPayment(orderRequest);
    verify(shipmentService, times(1)).createShipment(orderRequest);
    verify(orderService, times(1)).createOrder(any(Order.class));
    verify(productOrderService, times(1)).createProductOrders(orderRequest.getProductOrders(), orderResponse.getId());
    verify(productService, times(1)).details(productOrdersMock);

    assertEquals(orderResponseMock, orderResponse);
  }

  @Test
  void updateOrder_whenInputValid_returnOrderDetailResponse() {
    OrderUpdateRequest orderUpdateRequestMock = orderUpdateRequest;
    String orderId = "001";

    when(orderService.detail(orderId)).thenReturn(orderResponseMock);
    when(productOrderService.getIdByItemId("01", null, orderId))
          .thenReturn("poi01");
    when(productOrderService.getIdByItemId("00", null, orderId))
          .thenReturn("poi02");
    when(productOrderService.getById("poi01")).thenReturn(productOrderMock1);
    when(productOrderService.getById("poi02")).thenReturn(productOrderMock2);
    when(productOrderService.getByIds(anyList())).thenReturn(productOrderDetailResponsesMock);

    when(shipmentService.updateAddress(
          orderUpdateRequestMock.getFromAddress(),
          orderUpdateRequestMock.getCustomerId()))
          .thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, "addressId"));

    when(shipmentService.createShipmentOrigin(any()))
          .thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, new ShipmentResponse(
                "shipmentId",
                null,
                null,
                null,
                null,
                null,
                null
          )));

    when(paymentService.detail(orderResponseMock.getPaymentId()))
          .thenReturn(ResponseGeneral.of(HttpStatus.OK.value(), null, new PaymentResponse(
                "paymentId",
                "ct01",
                null,
                null
          )));

    OrderDetailResponse response = service.updateOrder(orderUpdateRequestMock, orderId);

    assertEquals(orderDetailResponse, response);
  }
}
