package com.lawman.order.server.service;

import com.lawman.order.client.dto.request.OrderStatusRequest;
import com.lawman.order.client.dto.response.OrderResponse;
import com.lawman.order.client.enums.OrderStatus;
import com.lawman.order.server.config.ConfigTest;
import com.lawman.order.server.entity.Order;
import com.lawman.order.server.exception.order.OrderNotFoundException;
import com.lawman.order.server.repository.OrderRepository;
import com.lawman.order.server.repository.ProductOrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@WebMvcTest(OrderService.class)
@ContextConfiguration(classes = {
      ConfigTest.class
})
class OrderServiceTest {
  @MockBean
  OrderRepository repository;

  @MockBean
  ProductOrderRepository productOrderRepository;

  @Autowired
  OrderService orderService;

  Order orderMock = new Order(
        "ct01",
        1800.0,
        null,
        "shipmentId",
        "paymentId",
        null,
        false
  );

  OrderResponse orderResponseMock = new OrderResponse(
        null,
        "ct01",
        null,
        1800.0,
        null,
        "shipmentId",
        "paymentId",
        null
  );

  @Test
  void createOrder_whenInputValid_returnOrderResponse() {
    Order order = orderMock;

    when(repository.save(order)).thenReturn(order);

    OrderResponse orderResponse = orderService.createOrder(order);

    assertEquals(orderResponse.getCustomerId(), orderResponseMock.getCustomerId());
    assertEquals(orderResponse.getTotalAmount(), orderResponseMock.getTotalAmount());
    assertEquals(orderResponse.getShipmentId(), orderResponseMock.getShipmentId());
    assertEquals(orderResponse.getPaymentId(), orderResponseMock.getPaymentId());
  }

  @Test
  void updateShipmentId_whenInputValid_shouldUpdateShipmentId() {
    String orderId = "01";
    String shipmentId = "shipmentId";

    orderService.updateShipmentId(orderId, shipmentId);

    verify(repository, times(1)).updateShipmentId(orderId, shipmentId);
  }

  @Test
  void detail_whenIdExisted_returnOrderResponse() {
    String id = "01";
    Order order = orderMock;
    order.setId(id);

    OrderResponse response = orderResponseMock;
    response.setId(id);

    when(repository.findById(id)).thenReturn(Optional.of(order));

    OrderResponse orderResponse = orderService.detail(id);

    assertEquals(response, orderResponse);
  }

  @Test
  void detail_whenIdDoesNotExist_throwException() {
    String id = "05";

    when(repository.findById(id)).thenReturn(Optional.empty());

    assertThrows(OrderNotFoundException.class, () ->
          orderService.detail(id)
    );
  }

  @Test
  void remove_whenIdExisted_shouldRemoveOrder() {
    String id = "01";

    when(repository.existsById(id)).thenReturn(true);

    orderService.remove(id);

    verify(repository, times(1)).removeById(id);
  }

  @Test
  void remove_whenIdDoesNotExisted_throwException() {
    String id = "05";

    when(repository.existsById(id)).thenReturn(false);

    assertThrows(OrderNotFoundException.class, () ->
      orderService.remove(id)
    );
  }

  @Test
  void updateOrderStatus_whenIdExisted_shouldUpdate() {
    String id = "01";
    OrderStatusRequest orderStatusRequest = new OrderStatusRequest(2);

    OrderStatus status = OrderStatus.of(2);

    when(repository.existsById(id)).thenReturn(true);

    orderService.updateOrderStatus(id, orderStatusRequest);

    verify(repository, times(1)).updateOrderStatus(id, status);
  }

  @Test
  void updateOrderStatus_whenIdDoesNotExisted_throwException() {
    String id = "05";
    OrderStatusRequest orderStatusRequest = new OrderStatusRequest(2);

    when(repository.existsById(id)).thenReturn(false);

    assertThrows(OrderNotFoundException.class, () ->
      orderService.updateOrderStatus(id, orderStatusRequest)
    );
  }
}
