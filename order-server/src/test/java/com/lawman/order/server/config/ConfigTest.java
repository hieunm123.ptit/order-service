package com.lawman.order.server.config;

import com.lawman.order.server.facade.OrderFacadeService;
import com.lawman.order.server.facade.impl.OrderFacadeServiceImpl;
import com.lawman.order.server.repository.OrderRepository;
import com.lawman.order.server.repository.ProductOrderRepository;
import com.lawman.order.server.service.OrderService;
import com.lawman.order.server.service.ProductOrderService;
import com.lawman.order.server.service.impl.OrderServiceImpl;
import com.lawman.order.server.service.impl.ProductOrderServiceImpl;
import com.lawman.order.server.service.routes.PaymentService;
import com.lawman.order.server.service.routes.ProductService;
import com.lawman.order.server.service.routes.ShipmentService;
import com.lawman.order.server.service.routes.UserService;
import com.lawman.order.server.service.routes.impl.PaymentServiceImpl;
import com.lawman.order.server.service.routes.impl.ProductServiceImpl;
import com.lawman.order.server.service.routes.impl.ShipmentServiceImpl;
import com.lawman.order.server.service.routes.impl.UserServiceImpl;
import jakarta.persistence.EntityManagerFactory;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.io.IOException;

@TestConfiguration
@EnableJpaRepositories(basePackages = {"com.lawman.order.server.repository"},
      entityManagerFactoryRef = "entityManagerFactoryTest",
      transactionManagerRef = "transactionManagerTest ")
@ComponentScan(basePackages = {"com.lawman.order.server.repository"})
@EnableTransactionManagement
public class ConfigTest {
  @Bean
  public DataSource dataSource() {
    EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
    return builder.setType(EmbeddedDatabaseType.H2).build();
  }

  @Bean
  public EntityManagerFactory entityManagerFactoryTest() throws IOException {

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    vendorAdapter.setGenerateDdl(true);

    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setJpaVendorAdapter(vendorAdapter);
    factory.setPackagesToScan("com.lawman.order.server.entity");
    factory.setDataSource(dataSource());
    factory.afterPropertiesSet();
    return factory.getObject();
  }

  @Bean
  public PlatformTransactionManager transactionManagerTest() throws IOException {
    JpaTransactionManager txManager = new JpaTransactionManager();
    txManager.setEntityManagerFactory(entityManagerFactoryTest());
    return txManager;
  }

  @Bean
  OrderService orderService(OrderRepository repository) {
    return new OrderServiceImpl(repository);
  }

  @Bean
  ProductOrderService productOrderService(ProductOrderRepository productOrderRepository) {
    return new ProductOrderServiceImpl(productOrderRepository);
  }

  @Bean
  UserService userService() {
    return new UserServiceImpl();
  }

  @Bean
  ProductService productService() {
    return new ProductServiceImpl();
  }

  @Bean
  PaymentService paymentService() {
    return new PaymentServiceImpl();
  }

  @Bean
  ShipmentService shipmentService() {
    return new ShipmentServiceImpl();
  }

  @Bean
  OrderFacadeService orderFacadeService(
        OrderService orderService,
        ProductOrderService productOrderService,
        UserService userService,
        ProductService productService,
        PaymentService paymentService,
        ShipmentService shipmentService) {
    return new OrderFacadeServiceImpl(
          orderService,
          productOrderService,
          userService,
          productService,
          paymentService,
          shipmentService);
  }
}
