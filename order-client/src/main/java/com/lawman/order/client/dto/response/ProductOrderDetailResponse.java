package com.lawman.order.client.dto.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class ProductOrderDetailResponse {
  private String id;
  private String orderId;
  private String productId;
  private String productItemId;
  private String name;
  private String color;
  private Integer quantity;
  private Double price;
  private String sellerId;

  public ProductOrderDetailResponse(
        String id,
        String orderId,
        String productId,
        String productItemId,
        Integer quantity,
        Double price,
        String sellerId) {
    this.id = id;
    this.orderId = orderId;
    this.productId = productId;
    this.productItemId = productItemId;
    this.quantity = quantity;
    this.price = price;
    this.sellerId = sellerId;
  }
}
