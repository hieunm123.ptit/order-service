package com.lawman.order.client.dto.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.lawman.order.client.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponse {
  private String id;
  private String customerId;
  private List<ProductOrderDetailResponse> productOrderDetails;
  private Double totalAmount;
  private String code;
  private String shipmentId;
  private String paymentId;
  private Integer status;

  public OrderResponse(
        String id,
        String customerId,
        Double totalAmount,
        String code,
        String shipmentId,
        String paymentId,
        OrderStatus status
  ) {
    this.id = id;
    this.customerId = customerId;
    this.totalAmount = totalAmount;
    this.code = code;
    this.shipmentId = shipmentId;
    this.paymentId = paymentId;
    this.status = status.getValue();
  }
}
