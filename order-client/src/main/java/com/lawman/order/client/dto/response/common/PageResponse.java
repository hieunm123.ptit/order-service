package com.lawman.order.client.dto.response.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Data
public class PageResponse<T> {
  private List<T> list;
  private int count;

  public static <T> PageResponse<T> of(List<T> data, Integer amount) {
    return new PageResponse<>(data, Objects.isNull(amount) ? 0 : amount);
  }
}
