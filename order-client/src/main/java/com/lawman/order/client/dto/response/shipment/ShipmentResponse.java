package com.lawman.order.client.dto.response.shipment;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentResponse {
  private String id;
  private String name;
  private Double price;
  private String method;
  private AddressResponse fromAddress;
  private AddressResponse toAddress;
  private String code;
}
