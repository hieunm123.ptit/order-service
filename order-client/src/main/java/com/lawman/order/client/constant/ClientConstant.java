package com.lawman.order.client.constant;

public class ClientConstant {
  private ClientConstant() {

  }
  public static final String CUSTOMER_ID_NOT_NULL = "com.lawman.order.client.dto.request.customerId.NotNull";
  public static final String SELLER_ID_NOT_NULL = "com.lawman.order.client.dto.request.sellerId.NotNull";
  public static final String LIST_PRODUCTS_NOT_NULL = "com.lawman.order.client.dto.request.productOrders.NotNull";
}
