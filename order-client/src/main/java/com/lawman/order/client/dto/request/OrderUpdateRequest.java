package com.lawman.order.client.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class OrderUpdateRequest {
  private String customerId;
  private List<ProductOrderRequest> productOrders;
  private AddressRequest fromAddress;
  private AddressRequest toAddress;
  private String shippingName;
  private String shippingMethod;
}
