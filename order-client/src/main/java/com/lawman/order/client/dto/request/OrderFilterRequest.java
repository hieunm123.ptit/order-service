package com.lawman.order.client.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class OrderFilterRequest {
  private List<String> productIds;
  private List<String> sellerIds;
  private String customerId;
  private Integer status;
  private Long startDate;
  private Long endDate;
}
