package com.lawman.order.client.enums;

public enum OrderStatus {
  ORDER_WAITING(0),
  ORDER_CREATED(1),
  ORDER_CANCELLED(2),
  ORDER_COMPLETED(3);

  private final int value;

  OrderStatus(int value) {
    this.value = value;
  }

  public static OrderStatus of(int value) {
    for (OrderStatus status : OrderStatus.values()) {
      if (status.value == value) {
        return status;
      }
    }
    return null;
  }

  public Integer getValue() {
    return value;
  }
}
