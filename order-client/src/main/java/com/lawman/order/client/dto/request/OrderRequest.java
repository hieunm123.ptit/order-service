package com.lawman.order.client.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static com.lawman.order.client.constant.ClientConstant.*;


@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {
  @NotNull(message = CUSTOMER_ID_NOT_NULL)
  private String customerId;
  @NotNull(message = LIST_PRODUCTS_NOT_NULL)
  private List<ProductOrderRequest> productOrders;
  private String shippingName;
  private String shippingMethod;
  private String paymentMethod;
}
