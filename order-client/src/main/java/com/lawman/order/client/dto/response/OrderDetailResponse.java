package com.lawman.order.client.dto.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.lawman.order.client.dto.response.payment.PaymentResponse;
import com.lawman.order.client.dto.response.shipment.ShipmentResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailResponse {
  private String id;
  private String customerId;
  private List<ProductOrderDetailResponse> productOrders;
  private Double totalAmount;
  private String code;
  private ShipmentResponse shipmentResponse;
  private PaymentResponse paymentResponse;
  private Integer status;
}
