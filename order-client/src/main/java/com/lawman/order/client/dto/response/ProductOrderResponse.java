package com.lawman.order.client.dto.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class ProductOrderResponse {
  private String id;
  private String orderId;
  private String productId;
  private String productItemId;
  private Integer quantity;
  private Double price;
  private String sellerId;
}
